import styles from './index.module.scss';

const Page = ({ name, children }: { name: string, children: any }) => {
	return (
		<main className={styles.Page}>
			<h1 className={styles.Page__title}>
				{name}
			</h1>
			<div className={styles.Page__content}>
				{children}
			</div>
		</main>
	)
};

export default Page;
