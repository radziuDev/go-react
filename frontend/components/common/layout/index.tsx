import React, { ReactNode } from 'react';
import styles from './index.module.scss'
import Header from '../header';
import Footer from '../footer';

const Layout = ({ children }: { children: ReactNode }) => (
	<div className={styles.Layout}>
		<Header />
		{children}
		<Footer />
	</div>
);

export default Layout;
