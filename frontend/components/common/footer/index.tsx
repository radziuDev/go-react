import styles from './index.module.scss';

const Footer = () => {
	const currentYear = new Date().getFullYear();
	return (
		<footer className={styles.Footer}>
			&copy; {currentYear} Job Board. All copyrights reserved.
		</footer>
	)
};

export default Footer;
