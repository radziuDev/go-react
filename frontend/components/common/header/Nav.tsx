import Link from 'next/link';
import styles from './Nav.module.scss';

const Nav = () => {
	const navLinks = [
		{ to: '/board', title: 'Board' },
		{ to: '/favorite', title: 'Favorite' },
		{ to: '/materials', title: 'Materials' },
	];

	return (
		<ul className={styles.Nav}>
			{navLinks.map((item) => (
				<li className={styles.Nav__item} key={item.to}>
					<Link href={item.to} className={styles.Nav__link}>
						{item.title}
					</Link>
				</li>
			))}
		</ul>
	)
};

export default Nav;
