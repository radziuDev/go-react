import Link from 'next/link';
import styles from './index.module.scss';
import Nav from './Nav';

const Header = () => (
 <div className={styles.Header}>
		<Link href="/" className={styles.Header__logo}>
			JobBoard
		</Link>
		<Nav />
 </div>
);

export default Header;
