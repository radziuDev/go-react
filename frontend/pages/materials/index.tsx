import React, { LegacyRef, useCallback, useContext, useEffect, useMemo, useReducer, useRef, useState } from 'react';
import styles from './index.module.scss';
import Page from '../../components/common/page';

const slowFunction = (num: number) => {
	for (let i = 0; i <= 2000000000; i++) { }
	return num * 2;
}

const Materials = () => {
	const [count, setCount] = useState(0);
	const [resource, setResource] = useState('post');

	useEffect(() => {
		console.log('on all re-renders')
		renderCount.current++;
		return () => {
			console.log('onEnd')
		}
	});

	useEffect(() => {
		console.log('on resource change')
		if (resource === 'post') {
			setCount(s => s + 1);
		}
	}, [resource]);

	useEffect(() => {
		console.log('onMount')

		return () => {
			console.log('onDestroy')
		}
	}, []);

	const [number, setNumber] = useState(0);
	const doubleNumber = useMemo(() => slowFunction(number), [number]);

	const [name, setName] = useState('');
	// not trigger rerender
	const renderCount = useRef(0);
	const refInput = useRef() as LegacyRef<HTMLInputElement>;

	const focus = () => {
		(refInput as Record<string, any>).current?.focus();
	}

	//--// export that and read in childs
	const ThemeContext = React.createContext({});
	const [theme, setTheme] = useState('dark');

	const ChildComponent = () => <GrandChildComponent />;
	const GrandChildComponent = () => {
		// import here
		const { theme, setTheme } = useContext(ThemeContext) as any;

		return (
			<>
				<div>The theme is {theme}</div>
				<button onClick={() => setTheme(theme === 'dark' ? 'light' : 'dark')}>
					Change To Light Theme
				</button>
			</>
		)
	}

	// -- //
	const reducer = (state: { count: number }, action: any) => ({ count: state.count + action });
	const [state, dispach]: [{ count: number }, any] = useReducer(reducer, { count: 0 }) as any;

	const increment = () => {
		dispach(12);
	}

	// - //

	const getItems = useCallback(() => {
		return state.count - 1;
	}, [state.count])

	const List = ({ num }: {num: () => number}) => {
		useEffect(() => {
			console.log("items are updated");
		});
		return ([0, 1, 2].map((item) => (<div key={item}>{num() + item}</div>)));
	}

	return (
		<Page name="Materials">
			<div className={styles.Home}>
				<h2>hooks</h2>
				<article>
					<h3>useState</h3>
					<button onClick={() => setCount(s => s - 1)}>
						-
					</button>
					<span>{count}</span>
					<button onClick={() => setCount(s => s + 1)}>
						+
					</button>
				</article>
				<br /><br />

				<article>
					<h3>useEffect</h3>
					<button onClick={() => setResource('post')}>post</button>
					<button onClick={() => setResource('users')}>users</button>
					<button onClick={() => setResource('comments')}>comments</button>
					{resource}
				</article>
				<br /><br />

				<article>
					<input type="number" value={number} onChange={e => setNumber(parseInt(e.target.value))} />
					{doubleNumber}
				</article>
				<br /><br />

				<article>
					<input value={name} onChange={e => setName(e.target.value)} />
					<br />name: {name}
					<div>I render {renderCount.current} time</div>
					<input ref={refInput} />
					<button onClick={focus}>x</button>
				</article>

				<article>
					<ThemeContext.Provider value={{ theme, setTheme }}>
						<ChildComponent />
					</ThemeContext.Provider>
				</article>

				<article>
					<button onClick={() => increment()}>+</button> {state.count}
				</article>

				<article>
					<List num={getItems} />
				</article>
			</div>
		</Page>
	);
}

export default Materials;
