#!/bin/bash

if [ ! -f .env ]; then
    cp .env.example .env
fi

cd ./backend/ && sh ./generate_all.sh
cd ..

docker-compose up --build -d
