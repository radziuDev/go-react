package user

import (
	pb "backend/services/user-service/api"
	"backend/services/user-service/pkg/database"
	"context"
	"fmt"
)

type ProspectService interface {
	GetUsers(ctx context.Context) (*pb.GetUsersResponse, error)
}

type ProspectServiceImpl struct {}

func (p *ProspectServiceImpl) GetUsers(ctx context.Context) (*pb.GetUsersResponse, error) {
	persons, err := database.GetUsers(ctx)
	if err != nil {
		return nil, err
	}

	var response []*pb.User
	for _, person := range persons {
		var cars []string
		for _, car := range person.Cars {
			cars = append(cars, fmt.Sprintf("%v %v", car.Producent, car.Model))
		}

		response = append(response, &pb.User{
			Name: person.Name,
			Cars: cars,
		})	
	}

	return &pb.GetUsersResponse{ Users: response }, nil
}
