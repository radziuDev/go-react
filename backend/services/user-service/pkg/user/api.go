package user

import (
	pb "backend/services/user-service/api"
	"context"
	"fmt"
	"log"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserServiceImpl struct{
	PService  ProspectService
	pb.UnimplementedUserServiceServer
}

func NewUserService() *UserServiceImpl {
	prospectService := &ProspectServiceImpl{}
	return &UserServiceImpl{
		PService: prospectService,
	}
}

func (s *UserServiceImpl) GetUsers(ctx context.Context, in *empty.Empty) (*pb.GetUsersResponse, error) {
	response, err := s.PService.GetUsers(ctx)
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("Internal Error: %v", err),
		)
	}

	return response, nil
}
