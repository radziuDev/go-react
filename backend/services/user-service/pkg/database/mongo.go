package database

import (
	"backend/services/user-service/pkg/config"
	"context"
	"log"
	"sync"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var Database *mongo.Database 

func InitMongo(wg *sync.WaitGroup) {
	wg.Done()

	clientOptions := options.Client().ApplyURI(config.Config.MongoUrl)
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	ctx := context.Background()
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(ctx)

	Database = client.Database("user-service")

	forever := make(chan error)
		log.Print("Database connected.")
	<- forever
}


