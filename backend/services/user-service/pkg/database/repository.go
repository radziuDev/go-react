package database

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Car struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	OwnerID   string             `bson:"ownerId,omitempty"`
	Producent string             `bson:"producent,omitempty"`
	Model     string             `bson:"model,omitempty"`
}

type Person struct {
	ID      primitive.ObjectID `bson:"_id,omitempty"`
	Name    string             `bson:"name,omitempty"`
	OwnerID string             `bson:"ownerId,omitempty"`
	Cars    []Car              `bson:"result,omitempty"`
}

func GetUsers(ctx context.Context) ([]Person, error) {
	collection := Database.Collection("users")
	pipeline := bson.A{
		bson.D{{Key: "$match", Value: bson.D{}}},
		bson.D{{Key: "$addFields", Value: bson.D{{Key: "ownerId", Value: bson.D{{Key: "$toString", Value: "$_id"}}}}}},
		bson.D{
			{Key: "$lookup", Value: bson.D{
				{Key: "from", Value: "cars"},
				{Key: "localField", Value: "ownerId"},
				{Key: "foreignField", Value: "ownerId"},
				{Key: "as", Value: "result"},
			}},
		},
	}

	var results []Person
	cur, err := collection.Aggregate(ctx, pipeline)
	if err != nil {
		log.Fatal(err)
	}
	defer cur.Close(ctx)

	for cur.Next(ctx) {
		var person Person
		if err := cur.Decode(&person); err != nil {
			return results, nil
		}
		results = append(results, person)
	}

	return results, nil
}
