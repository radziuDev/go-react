package main

import (
	pb "backend/services/user-service/api"
	"backend/services/user-service/pkg/config"
	"backend/services/user-service/pkg/database"
	"fmt"
	"log"
	"net"
	"sync"

	grpcImpl "backend/services/user-service/pkg/user"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type server struct{
	grpcImpl.UserServiceImpl
}

func main() {
	config.LoadConfigs()

	var wg sync.WaitGroup
	wg.Add(2)
	go startServer(&wg)
	go database.InitMongo(&wg)
	wg.Wait()
}

func startServer(wg *sync.WaitGroup) {
	defer wg.Done()

	listen, err := net.Listen("tcp", fmt.Sprintf(":%v", config.Config.UserServicePort))
	if err != nil {
		panic(err)
	}

	grpcServer := grpc.NewServer()
	reflection.Register(grpcServer)

	newServer := grpcImpl.NewUserService()
	pb.RegisterUserServiceServer(grpcServer, newServer)

	log.Printf("Server is listening on port :%v", config.Config.UserServicePort)
	if err := grpcServer.Serve(listen); err != nil {
		panic(err)
	}
}
