package main

import (
	pb "backend/services/scraper-service/api"
	"backend/services/scraper-service/pkg/config"
	"fmt"
	"log"
	"net"

	grpcImpl "backend/services/scraper-service/pkg/scraper"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type server struct{
	grpcImpl.ScraperServiceImpl
}

func main() {
	config.LoadConfigs()

	listen, err := net.Listen("tcp", fmt.Sprintf(":%v", config.Config.ScraperServicePort))
	if err != nil {
		panic(err)
	}

	grpcServer := grpc.NewServer()
	reflection.Register(grpcServer)

	newServer := grpcImpl.NewScraperService()
	pb.RegisterScraperServiceServer(grpcServer, newServer)

	log.Printf("Server is listening on port :%v", config.Config.ScraperServicePort)
	if err := grpcServer.Serve(listen); err != nil {
		panic(err)
	}
}
