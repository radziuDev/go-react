package scraper

import (
	pb "backend/services/scraper-service/api"
	"context"
)

type ScraperServiceImpl struct{
	pb.UnimplementedScraperServiceServer
}

func NewScraperService() *ScraperServiceImpl {
	return &ScraperServiceImpl{}
}

func (s *ScraperServiceImpl) Add(ctx context.Context, req *pb.AddRequest) (*pb.AddResponse, error) {
	response := pb.AddResponse{ Result: req.A + req.B }
	return &response, nil
}
