package config

import (
	"log"
	"os"
	"strconv"
)

type ConfigStruct struct {
	RestPort           int
	ScraperServicePort int
	UserServicePort    int
}

var Config ConfigStruct = ConfigStruct{}

func LoadConfigs() {
	var err error

	Config.RestPort, err = strconv.Atoi(os.Getenv("REST_API_PORT"))
	if err != nil {
		log.Fatal("Can't set restPort variable.")
	}
	Config.ScraperServicePort, err = strconv.Atoi(os.Getenv("SCRAPER_SERVICE_PORT"))
	if err != nil {
		log.Fatal("Can't set scraperServicePort variable.")
	}
	Config.UserServicePort, err = strconv.Atoi(os.Getenv("USER_SERVICE_PORT"))
	if err != nil {
		log.Fatal("Can't set userServicePort variable.")
	}
}
