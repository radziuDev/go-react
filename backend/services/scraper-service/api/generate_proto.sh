#!/bin/sh

echo "generating go proto"
protoc --go_out=. -I . -I ../../../ -I ../../../googleapis/ --go-grpc_out=. --go-grpc_opt=paths=source_relative --go_opt=paths=source_relative scraper-service.proto

echo "generating rest gateway"
protoc -I . -I ../../../ -I ../../../googleapis/ --grpc-gateway_out . --grpc-gateway_opt logtostderr=true --grpc-gateway_opt paths=source_relative --grpc-gateway_opt generate_unbound_methods=true scraper-service.proto

echo "generating rest docs (swagger)"
protoc -I . -I ../../../ -I ../../../googleapis/ --openapiv2_out ./openapi --openapiv2_opt logtostderr=true scraper-service.proto
