package services

import (
	"context"
	"log"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ErrorMessage struct {
	Code    int32
	Message string
}

func CreateService(ctx context.Context, gw *runtime.ServeMux, endpoint string) runtime.ServeMuxOption {
	customProtoErrorHandler := func(ctx context.Context, mux *runtime.ServeMux, marshaler runtime.Marshaler, w http.ResponseWriter, r *http.Request, err error) {
		statusErr, ok := status.FromError(err)
		if !ok {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		var httpStatusCode int
		switch statusErr.Code() {
		case codes.NotFound:
			httpStatusCode = http.StatusNotFound
		default:
			httpStatusCode = http.StatusInternalServerError
		}

		jsonBody, err := marshaler.Marshal(ErrorMessage{
			Code:    int32(httpStatusCode),
			Message: statusErr.Message(),
		})
		if err != nil {
			log.Printf("Failed to marshal error response: %v", err)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(httpStatusCode)
		w.Write(jsonBody)
	}

	return runtime.WithProtoErrorHandler(customProtoErrorHandler)
}

