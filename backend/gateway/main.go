package main

import (
	"backend/gateway/pkg/config"
	"backend/gateway/pkg/services"
	"context"
	"fmt"
	"log"
	"net/http"

	pbScraper "backend/services/scraper-service/api"
	pbUser "backend/services/user-service/api"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"
)

func main() {
	config.LoadConfigs()

	opts := []grpc.DialOption{grpc.WithInsecure()}
	mux := runtime.NewServeMux()
	ctx := context.Background()

	scraperService := services.CreateService(ctx, mux, fmt.Sprintf("scraper-service:%v", config.Config.ScraperServicePort))
	userService := services.CreateService(ctx, mux, fmt.Sprintf("user-service:%v", config.Config.UserServicePort))

	gw := runtime.NewServeMux(
		scraperService,
		userService,
	)

	pbScraper.RegisterScraperServiceHandlerFromEndpoint(ctx, gw, fmt.Sprintf("scraper-service:%v", config.Config.ScraperServicePort), opts)
	pbUser.RegisterUserServiceHandlerFromEndpoint(ctx, gw, fmt.Sprintf("user-service:%v", config.Config.UserServicePort), opts)

	http.Handle("/", gw)

	log.Printf("HTTP server is listening on port :%v", config.Config.RestPort)
	if err := http.ListenAndServe(fmt.Sprintf(":%v", config.Config.RestPort), nil); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
